package ru.ermolaev.tm.event;

public class ConsoleEvent {

    private String name;

    public ConsoleEvent() {
    }

    public ConsoleEvent(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

}
