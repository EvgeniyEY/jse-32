package ru.ermolaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.TaskEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;

@Component
public class TasksClearListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Delete all tasks.";
    }

    @Autowired
    public TasksClearListener(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@tasksClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[CLEAR TASKS]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        taskEndpoint.clearTasks(session);
        System.out.println("[COMPLETE]");
    }

}
