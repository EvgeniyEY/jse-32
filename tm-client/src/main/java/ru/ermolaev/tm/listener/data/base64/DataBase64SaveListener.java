package ru.ermolaev.tm.listener.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.data.AbstractDataListener;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataBase64SaveListener extends AbstractDataListener {

    @NotNull
    @Override
    public String command() {
        return "data-base64-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Autowired
    public DataBase64SaveListener(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@dataBase64SaveListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.saveBase64(session);
        System.out.println("[OK]");
    }

}
