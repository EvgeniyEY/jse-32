package ru.ermolaev.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.event.ConsoleEvent;

public abstract class AbstractListener {

    protected ISessionService sessionService;

    @Nullable
    public abstract String command();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public AbstractListener() {
    }

    @Autowired
    public AbstractListener(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    public abstract void handler(final ConsoleEvent event) throws Exception;

}
