package ru.ermolaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;

import java.util.List;

@Component
public class HelpListener extends AbstractListener {

    @Autowired
    private List<AbstractListener> commandList;

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    @EventListener(condition = "(@helpListener.command() == #event.name) || (@helpListener.arg() == #event.name)")
    public void handler(final ConsoleEvent event) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener command: commandList) {
            System.out.println(command.command()
                    + (command.arg() == null ? ": " : ", " + command.arg() + ": ")
                    + command.description());
        }
    }

}
