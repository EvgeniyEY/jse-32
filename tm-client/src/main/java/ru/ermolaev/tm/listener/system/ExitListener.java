package ru.ermolaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;

@Component
public class ExitListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "exit";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Exit from application.";
    }

    @Override
    @EventListener(condition = "@exitListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.exit(0);
    }

}
