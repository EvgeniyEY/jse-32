package ru.ermolaev.tm.listener.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.data.AbstractDataListener;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataBinaryClearListener extends AbstractDataListener {

    @NotNull
    @Override
    public String command() {
        return "data-bin-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Autowired
    public DataBinaryClearListener(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@dataBinaryClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE BINARY FILE]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.clearBinaryFile(session);
        System.out.println("[OK]");
    }

}
