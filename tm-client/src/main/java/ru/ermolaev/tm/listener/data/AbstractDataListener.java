package ru.ermolaev.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;

@Component
public abstract class AbstractDataListener extends AbstractListener {

    protected AdminDataEndpoint adminDataEndpoint;

    @Autowired
    public AbstractDataListener(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.adminDataEndpoint = adminDataEndpoint;
    }

}
