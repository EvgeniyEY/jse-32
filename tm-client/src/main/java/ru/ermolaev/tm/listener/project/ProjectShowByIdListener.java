package ru.ermolaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.ProjectDTO;
import ru.ermolaev.tm.endpoint.ProjectEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Autowired
    public ProjectShowByIdListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(projectEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @NotNull final ProjectDTO project = projectEndpoint.findProjectById(session, id);
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[COMPLETE]");
    }

}
