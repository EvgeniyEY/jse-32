package ru.ermolaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.UserEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserMiddleNameUpdateListener extends AbstractUserListener {

    @NotNull
    @Override
    public String command() {
        return "user-update-middle-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user middle name.";
    }

    @Autowired
    public UserMiddleNameUpdateListener(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(userEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@userMiddleNameUpdateListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME:");
        @Nullable final String newMiddleName = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        userEndpoint.updateUserMiddleName(session, newMiddleName);
        System.out.println("[OK]");
    }

}
