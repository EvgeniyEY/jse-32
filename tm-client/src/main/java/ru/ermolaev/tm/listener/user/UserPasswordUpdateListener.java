package ru.ermolaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.SessionEndpoint;
import ru.ermolaev.tm.endpoint.UserEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserPasswordUpdateListener extends AbstractUserListener {

    private final SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String command() {
        return "user-update-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user password.";
    }

    @Autowired
    public UserPasswordUpdateListener(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ISessionService sessionService,
            @NotNull final SessionEndpoint sessionEndpoint
    ) {
        super(userEndpoint, sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @Override
    @EventListener(condition = "@userPasswordUpdateListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        userEndpoint.updateUserPassword(session, newPassword);
        System.out.println("[PASSWORD CHANGED]");
        sessionEndpoint.closeSession(session);
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

}
