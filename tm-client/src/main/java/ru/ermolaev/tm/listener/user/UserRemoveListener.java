package ru.ermolaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.AdminUserEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserRemoveListener extends AbstractAdminListener {

    @NotNull
    @Override
    public String command() {
        return "user-remove";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user.";
    }

    @Autowired
    public UserRemoveListener(
            @NotNull final AdminUserEndpoint adminUserEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminUserEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@userRemoveListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminUserEndpoint.removeUserByLogin(session, login);
        System.out.println("[OK]");
    }

}
