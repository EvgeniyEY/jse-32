package ru.ermolaev.tm.listener.data.json.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.data.AbstractDataListener;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataJsonJaxbLoadListener extends AbstractDataListener {

    @NotNull
    @Override
    public String command() {
        return "data-json-jb-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json (Jax-B) file.";
    }

    @Autowired
    public DataJsonJaxbLoadListener(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@dataJsonJaxbLoadListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[DATA JSON (JAX-B) LOAD]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.loadJsonByJaxb(session);
        System.out.println("[OK]");
    }

}
