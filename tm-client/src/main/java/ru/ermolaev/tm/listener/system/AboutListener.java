package ru.ermolaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;

@Component
public class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    @EventListener(condition = "(@aboutListener.command() == #event.name) || (@aboutListener.arg() == #event.name)")
    public void handler(final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Evgeniy Ermolaev");
        System.out.println("E-MAIL: ermolaev.evgeniy.96@yandex.ru");
    }

}
