package ru.ermolaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.SessionEndpoint;

@Component
public class ServerInfoListener extends AbstractListener {

    private final SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String command() {
        return "server-info";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about connecting to server.";
    }

    @Autowired
    public ServerInfoListener(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @Override
    @EventListener(condition = "@serverInfoListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[SERVER INFO]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @NotNull final String host = sessionEndpoint.getServerHost(session);
        @NotNull final Integer port = sessionEndpoint.getServerPort(session);
        System.out.println("HOST: " + host);
        System.out.println("PORT: " + port);
        System.out.println("[OK]");
    }

}
