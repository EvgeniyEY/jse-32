package ru.ermolaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.SessionEndpoint;
import ru.ermolaev.tm.service.SessionService;

@Component
public class UserLogoutListener extends AbstractListener {

    private final SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String command() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account.";
    }

    @Autowired
    public UserLogoutListener(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final SessionService sessionService
    ) {
        super(sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @Override
    @EventListener(condition = "@userLogoutListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[LOGOUT]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        sessionEndpoint.closeSession(session);
        sessionService.clearCurrentSession();
        System.out.println("[OK]");
    }

}
