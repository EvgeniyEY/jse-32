package ru.ermolaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.UserEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserLastNameUpdateListener extends AbstractUserListener {

    @NotNull
    @Override
    public String command() {
        return "user-update-last-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user last name.";
    }

    @Autowired
    public UserLastNameUpdateListener(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(userEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@userLastNameUpdateListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME:");
        @Nullable final String newLastName = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        userEndpoint.updateUserLastName(session, newLastName);
        System.out.println("[OK]");
    }

}
