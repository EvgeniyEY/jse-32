package ru.ermolaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;

import java.util.List;

@Component
public class CommandsListener extends AbstractListener {

    @Autowired
    private List<AbstractListener> commandList;

    @NotNull
    @Override
    public String command() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application's commands.";
    }

    @Override
    @EventListener(condition = "(@commandsListener.command() == #event.name) || (@commandsListener.arg() == #event.name)")
    public void handler(final ConsoleEvent event) {
        for (@NotNull final AbstractListener command: commandList) System.out.println(command.command());
    }

}
