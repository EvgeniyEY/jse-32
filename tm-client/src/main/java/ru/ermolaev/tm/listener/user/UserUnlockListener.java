package ru.ermolaev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.AdminUserEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserUnlockListener extends AbstractAdminListener {

    @NotNull
    @Override
    public String command() {
        return "user-unlock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user.";
    }

    @Autowired
    public UserUnlockListener(
            @NotNull final AdminUserEndpoint adminUserEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminUserEndpoint, sessionService);
    }

    @Override
    @EventListener(condition = "@userUnlockListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminUserEndpoint.unlockUserByLogin(session, login);
        System.out.println("[OK]");
    }

}
