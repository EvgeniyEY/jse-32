!#/usr/bin/env bash

if [ ! -f tm-server.pid ]; then
	echo "tm-server pid not found"
	exit 1;
fi

echo 'process with pid '$(cat tm-server.pid)' was killed';
kill -9 $(cat tm-server.pid)
rm tm-server.pid
