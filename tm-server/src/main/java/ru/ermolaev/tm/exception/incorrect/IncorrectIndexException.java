package ru.ermolaev.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() {
        super("Error! Index is incorrect.");
    }

    public IncorrectIndexException(@NotNull final String value) {
        super("Error! This value [" + value + "] is not number.");
    }

}
