package ru.ermolaev.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

public final class UnknownLoginException extends AbstractException {

    public UnknownLoginException() {
        super("Error! This login does not exist.");
    }

    public UnknownLoginException(@NotNull final String login) {
        super("Error! This login [" + login + "] does not exist.");
    }

}
