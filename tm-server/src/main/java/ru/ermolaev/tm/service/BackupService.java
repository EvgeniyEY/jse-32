package ru.ermolaev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermolaev.tm.api.service.IBackupService;
import ru.ermolaev.tm.api.service.IDomainService;
import ru.ermolaev.tm.constant.DataConstant;
import ru.ermolaev.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class BackupService implements IBackupService {

    private final IDomainService domainService;

    @Autowired
    public BackupService(@NotNull final IDomainService domainService) {
        this.domainService = domainService;
    }

    public void saveXmlByJaxb() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = prepareFile(DataConstant.FILE_XML_JB);

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.marshal(domain, file);
    }

    public void loadXmlByJaxb() throws Exception {
        @NotNull final File xmlFile = new File(DataConstant.FILE_XML_JB);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(xmlFile);

        domainService.load(domain);
    }

    public void clearXmlFileJaxb() throws Exception {
        clearFile(DataConstant.FILE_XML_JB);
    }

    public void saveXmlByFasterXml() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = prepareFile(DataConstant.FILE_XML_FX);

        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xmlData = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(xmlData.getBytes());
        }
    }

    public void loadXmlByFasterXml() throws Exception {
        @NotNull final String xmlData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML_FX)));
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(xmlData, Domain.class);

        domainService.load(domain);
    }

    public void clearXmlFileFasterXml() throws Exception {
        clearFile(DataConstant.FILE_XML_FX);
    }

    public void saveJsonByJaxb() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = prepareFile(DataConstant.FILE_JSON_JB);

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        jaxbMarshaller.marshal(domain, file);
    }

    public void loadJsonByJaxb() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File file = new File(DataConstant.FILE_JSON_JB);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);

        domainService.load(domain);
    }

    public void clearJsonFileJaxb() throws Exception {
        clearFile(DataConstant.FILE_JSON_JB);
    }

    public void saveJsonByFasterXml() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = prepareFile(DataConstant.FILE_JSON_FX);

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String jsonData = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(jsonData.getBytes());
        }
    }

    public void loadJsonByFasterXml() throws Exception {
        @NotNull final String jsonData = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON_FX)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(jsonData, Domain.class);
        domainService.load(domain);
    }

    public void clearJsonFileFasterXml() throws Exception {
        clearFile(DataConstant.FILE_JSON_FX);
    }

    public void saveBinary() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = prepareFile(DataConstant.FILE_BINARY);

        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
             @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
        ) {
            objectOutputStream.writeObject(domain);
        }
    }

    public void loadBinary() throws Exception {
        try (@NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
             @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            domainService.load(domain);
        }
    }

    public void clearBinaryFile() throws Exception {
        clearFile(DataConstant.FILE_BINARY);
    }

    public void saveBase64() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = prepareFile(DataConstant.FILE_BASE64);

        try (@NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
             @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file)
        ) {
            objectOutputStream.writeObject(domain);
            @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
            @NotNull final String base64 = new BASE64Encoder().encode(bytes);
            fileOutputStream.write(base64.getBytes());
        }
    }

    public void loadBase64() throws Exception {
        @NotNull final String base64date = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_BASE64)));
        @NotNull final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64date);

        try (@NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
             @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            domainService.load(domain);
        }
    }

    public void clearBase64File() throws Exception {
        clearFile(DataConstant.FILE_BASE64);
    }

    private void clearFile(@Nullable final String fileName) throws Exception {
        if (fileName == null || fileName.isEmpty()) return;
        @NotNull final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
    }

    @NotNull
    private File prepareFile(final String fileName) throws Exception {
        @NotNull final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        return file;
    }

    @NotNull
    private Domain prepareData() {
        @NotNull final Domain domain = new Domain();
        domainService.export(domain);
        return domain;
    }

}
