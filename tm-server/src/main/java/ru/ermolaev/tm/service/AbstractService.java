package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.service.IDatabaseConnectionService;
import ru.ermolaev.tm.api.service.IService;
import ru.ermolaev.tm.entity.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IDatabaseConnectionService databaseConnectionService;

    public AbstractService(@NotNull final IDatabaseConnectionService databaseConnectionService) {
        this.databaseConnectionService = databaseConnectionService;
    }

}