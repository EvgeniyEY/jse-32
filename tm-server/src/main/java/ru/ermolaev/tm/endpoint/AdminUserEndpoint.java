package ru.ermolaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ermolaev.tm.api.endpoint.IAdminUserEndpoint;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
@NoArgsConstructor
public final class AdminUserEndpoint implements IAdminUserEndpoint {

    private ISessionService sessionService;

    private IUserService userService;

    @Autowired
    public AdminUserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService
    ) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @Override
    @WebMethod
    public void createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) throws Exception {
        userService.create(login, password, email);
    }

    @Override
    @WebMethod
    public void createUserWithRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final Role role
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.create(login, password, role);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        userService.removeOneByLogin(login);
    }

}
