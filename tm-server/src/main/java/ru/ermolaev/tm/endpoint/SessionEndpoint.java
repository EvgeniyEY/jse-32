package ru.ermolaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ermolaev.tm.api.endpoint.ISessionEndpoint;
import ru.ermolaev.tm.api.service.IPropertyService;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.dto.Fail;
import ru.ermolaev.tm.dto.Result;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.dto.Success;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
@NoArgsConstructor
public final class SessionEndpoint implements ISessionEndpoint {

    private ISessionService sessionService;

    private IPropertyService propertyService;

    @Autowired
    public SessionEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IPropertyService propertyService
    ) {
        this.sessionService = sessionService;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception {
        return sessionService.open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        try {
            sessionService.close(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeAllSessionsForUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        try {
            sessionService.closeAll(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public String getServerHost(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return propertyService.getServerHost();
    }

    @Nullable
    @Override
    @WebMethod
    public Integer getServerPort(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return propertyService.getServerPort();
    }

}
