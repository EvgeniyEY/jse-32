package ru.ermolaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ermolaev.tm.api.endpoint.IUserEndpoint;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
@NoArgsConstructor
public final class UserEndpoint implements IUserEndpoint {

    private ISessionService sessionService;

    private IUserService userService;

    @Autowired
    public UserEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IUserService userService
    ) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) throws Exception {
        sessionService.validate(sessionDTO);
        userService.updatePassword(sessionDTO.getUserId(), newPassword);
    }

    @Override
    @WebMethod
    public void updateUserFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newFirstName", partName = "newFirstName") @Nullable final String newFirstName
    ) throws Exception {
        sessionService.validate(sessionDTO);
        userService.updateUserFirstName(sessionDTO.getUserId(), newFirstName);
    }

    @Override
    @WebMethod
    public void updateUserMiddleName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newMiddleName", partName = "newMiddleName") @Nullable final String newMiddleName
    ) throws Exception {
        sessionService.validate(sessionDTO);
        userService.updateUserMiddleName(sessionDTO.getUserId(), newMiddleName);
    }

    @Override
    @WebMethod
    public void updateUserLastName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newLastName", partName = "newLastName") @Nullable final String newLastName
    ) throws Exception {
        sessionService.validate(sessionDTO);
        userService.updateUserLastName(sessionDTO.getUserId(), newLastName);
    }

    @Override
    @WebMethod
    public void updateUserEmail(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newEmail", partName = "newEmail") @Nullable final String newEmail
    ) throws Exception {
        sessionService.validate(sessionDTO);
        userService.updateUserEmail(sessionDTO.getUserId(), newEmail);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findUserProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        return sessionService.getUser(sessionDTO);
    }

}
