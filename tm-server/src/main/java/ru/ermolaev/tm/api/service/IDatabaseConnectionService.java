package ru.ermolaev.tm.api.service;

import javax.persistence.EntityManager;

public interface IDatabaseConnectionService {

    EntityManager getEntityManager();

}
