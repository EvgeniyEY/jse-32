package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IService<T> {

    void persist(@Nullable T t);

    void merge(@Nullable T t);

    void remove(@Nullable T t);

}
