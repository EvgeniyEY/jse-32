package ru.ermolaev.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.ermolaev.tm")
public class ApplicationConfiguration {

}
