package ru.ermolaev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Project;

import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date completeDate;

    @Nullable
    private Date creationDate;

    @Override
    public String toString() {
        return "Project [" +
                "Name='" + name + '\'' +
                ", Description='" + description + '\'' +
                ']';
    }

    @Nullable
    public static ProjectDTO toDTO(@Nullable final Project project) {
        if (project == null) return null;
        return new ProjectDTO(project);
    }

    @NotNull
    public static List<ProjectDTO> toDTO(@Nullable final Collection<Project> projects) {
        if (projects == null || projects.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            result.add(new ProjectDTO(project));
        }
        return result;
    }

    public ProjectDTO(@Nullable final Project project) {
        if (project == null) return;
        id = project.getId();
        name = project.getName();
        description = project.getDescription();
        if (project.getUser() != null) userId = project.getUser().getId();
        if (project.getCreationDate() != null) creationDate = project.getCreationDate();
        if (project.getStartDate() != null) startDate = project.getStartDate();
        if (project.getCompleteDate() != null) completeDate = project.getCompleteDate();
    }

}
