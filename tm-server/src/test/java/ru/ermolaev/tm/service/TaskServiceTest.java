package ru.ermolaev.tm.service;

public class TaskServiceTest {

    /*
        Требуется рефакторинг в связи с переходом на новую моель хранения данных
    */

//    final private ITaskRepository taskRepository = new TaskRepository();
//
//    final private ITaskService taskService = new TaskService(taskRepository);
//
//    @Test
//    public void addTaskTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskService.addTask("100", taskDTO);
//        Assert.assertEquals(1, taskService.findAll().size());
//        taskService.addTask("100", null);
//        Assert.assertEquals(1, taskService.findAll().size());
//    }
//
//    @Test
//    public void createTaskTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        taskService.createTask("100", "task");
//        final TaskDTO taskDTO = taskService.findTaskByName("100", "task");
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals("100", taskDTO.getUserId());
//        Assert.assertEquals("task", taskDTO.getName());
//        Assert.assertEquals(1, taskService.findAll().size());
//    }
//
//    @Test
//    public void createTaskWithDescriptionTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        taskService.createTask("100", "task", "first task");
//        final TaskDTO taskDTO = taskService.findTaskByName("100", "task");
//        Assert.assertNotNull(taskDTO);
//        Assert.assertEquals("100", taskDTO.getUserId());
//        Assert.assertEquals("task", taskDTO.getName());
//        Assert.assertEquals("first task", taskDTO.getDescription());
//        Assert.assertEquals(1, taskService.findAll().size());
//    }
//
//    @Test
//    public void findAllTasksTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        for (int i = 0; i < 10; i++) {
//            taskService.addTask("100", new TaskDTO());
//        }
//        taskService.createTask("200","task1");
//        Assert.assertEquals(11, taskService.findAll().size());
//        Assert.assertEquals(10, taskService.findAllTasks("100").size());
//    }
//
//    @Test
//    public void findTaskByIdTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskService.addTask("100", taskDTO);
//        final TaskDTO tempTaskDTO = taskService.findTaskById("100", taskDTO.getId());
//        Assert.assertNotNull(tempTaskDTO);
//        Assert.assertEquals(taskDTO.getId(), tempTaskDTO.getId());
//    }
//
//    @Test
//    public void findTaskByIndexTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskService.addTask("100", taskDTO);
//        final TaskDTO tempTaskDTO = taskService.findTaskByIndex("100", 0);
//        Assert.assertNotNull(tempTaskDTO);
//        Assert.assertEquals(taskDTO.getId(), tempTaskDTO.getId());
//    }
//
//    @Test
//    public void findTaskByNameTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskDTO.setName("task1");
//        taskService.addTask("100", taskDTO);
//        final TaskDTO tempTaskDTO = taskService.findTaskByName("100", taskDTO.getName());
//        Assert.assertNotNull(tempTaskDTO);
//        Assert.assertEquals(taskDTO.getId(), tempTaskDTO.getId());
//    }
//
//    @Test
//    public void findAllTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        for (int i = 0; i < 10; i++) {
//            taskService.addTask("100", new TaskDTO());
//        }
//        Assert.assertEquals(10, taskService.findAll().size());
//    }
//
//    @Test
//    public void updateTaskByIdTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskDTO.setName("task1");
//        taskDTO.setDescription("task1");
//        taskService.addTask("100", taskDTO);
//        taskService.updateTaskById("100", taskDTO.getId(), "newName", "newDesc");
//        Assert.assertEquals(1, taskService.findAll().size());
//        final TaskDTO tempTaskDTO = taskService.findTaskByName("100", "newName");
//        Assert.assertNotNull(tempTaskDTO);
//        Assert.assertEquals(taskDTO.getId(), tempTaskDTO.getId());
//        Assert.assertEquals("100", tempTaskDTO.getUserId());
//        Assert.assertEquals("newName", tempTaskDTO.getName());
//        Assert.assertEquals("newDesc", tempTaskDTO.getDescription());
//    }
//
//    @Test
//    public void updateTaskByIndexTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskDTO.setName("task1");
//        taskDTO.setDescription("task1");
//        taskService.addTask("100", taskDTO);
//        taskService.updateTaskByIndex("100", 0, "newName", "newDesc");
//        Assert.assertEquals(1, taskService.findAll().size());
//        final TaskDTO tempTaskDTO = taskService.findTaskByName("100", "newName");
//        Assert.assertNotNull(tempTaskDTO);
//        Assert.assertEquals(taskDTO.getId(), tempTaskDTO.getId());
//        Assert.assertEquals("100", tempTaskDTO.getUserId());
//        Assert.assertEquals("newName", tempTaskDTO.getName());
//        Assert.assertEquals("newDesc", tempTaskDTO.getDescription());
//    }
//
//    @Test
//    public void removeTaskByIdTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskService.addTask("100", taskDTO);
//        Assert.assertEquals(1, taskService.findAll().size());
//        taskService.removeTaskById("100", taskDTO.getId());
//        Assert.assertEquals(0, taskService.findAll().size());
//    }
//
//    @Test
//    public void removeTaskByIndexTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        taskService.addTask("100", new TaskDTO());
//        Assert.assertEquals(1, taskService.findAll().size());
//        taskService.removeTaskByIndex("100", 0);
//        Assert.assertEquals(0, taskService.findAll().size());
//    }
//
//    @Test
//    public void removeTaskByNameTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskDTO.setName("task1");
//        taskService.addTask("100", taskDTO);
//        Assert.assertEquals(1, taskService.findAll().size());
//        taskService.removeTaskByName("100", taskDTO.getName());
//        Assert.assertEquals(0, taskService.findAll().size());
//    }
//
//    @Test
//    public void removeTaskTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        final TaskDTO taskDTO = new TaskDTO();
//        taskService.addTask("100", taskDTO);
//        Assert.assertEquals(1, taskService.findAll().size());
//        taskService.removeTask("100", taskDTO);
//        Assert.assertEquals(0, taskService.findAll().size());
//
//    }
//
//    @Test
//    public void removeAllTasksTest() throws Exception {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        for (int i = 0; i < 5; i++) {
//            taskService.addTask("100", new TaskDTO());
//        }
//        for (int i = 0; i < 5; i++) {
//            taskService.addTask("200", new TaskDTO());
//        }
//        Assert.assertEquals(10, taskService.findAll().size());
//        taskService.removeAllTasks("100");
//        Assert.assertEquals(5, taskService.findAll().size());
//
//    }
//
//    @Test
//    public void loadCollectionTest() {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        List<TaskDTO> taskDTOS = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            taskDTOS.add(new TaskDTO());
//        }
//        taskService.load(taskDTOS);
//        Assert.assertEquals(5, taskService.findAll().size());
//    }
//
//    @Test
//    public void loadVarargsTest() {
//        Assert.assertTrue(taskService.findAll().isEmpty());
//        TaskDTO[] taskDTOS = new TaskDTO[5];
//        for (int i = 0; i < 5; i++) {
//            taskDTOS[i] = new TaskDTO();
//        }
//        taskService.load(taskDTOS);
//        Assert.assertEquals(5, taskService.findAll().size());
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void createTaskWithEmptyUserIdTest() throws Exception {
//        taskService.createTask("", "task");
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void createTaskWithNullUserIdTest() throws Exception {
//        taskService.createTask(null, "task");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void createTaskWithEmptyNameTest() throws Exception {
//        taskService.createTask("100", "");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void createTaskWithNullNameTest() throws Exception {
//        taskService.createTask("100", null);
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void createTaskWithEmptyDescriptionTest() throws Exception {
//        taskService.createTask("100", "task", "");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void createTaskWithNullDescriptionTest() throws Exception {
//        taskService.createTask("100", "task", null);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void addTaskWithEmptyUserIdTest() throws Exception {
//        taskService.addTask("", new TaskDTO());
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void addTaskWithNullUserIdTest() throws Exception {
//        taskService.addTask(null, new TaskDTO());
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void findAllTasksWithEmptyUserIdTest() throws Exception {
//        taskService.findAllTasks("");
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void findAllTasksWithNullUserIdTest() throws Exception {
//        taskService.findAllTasks(null);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void findTaskByIdWithEmptyUserIdTest() throws Exception {
//        taskService.findTaskById("", "task");
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void findTaskByIdWithNullUserIdTest() throws Exception {
//        taskService.findTaskById(null, "task");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void findTaskByIdWithEmptyIdTest() throws Exception {
//        taskService.findTaskById("100", "");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void findTaskByIdWithNullIdTest() throws Exception {
//        taskService.findTaskById("100", null);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void findTaskByIndexWithEmptyUserIdTest() throws Exception {
//        taskService.findTaskByIndex("", 0);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void findTaskByIndexWithNullUserIdTest() throws Exception {
//        taskService.findTaskByIndex(null, 0);
//    }
//
//    @Test(expected = IncorrectIndexException.class)
//    public void findTaskByIndexWithNullIndexTest() throws Exception {
//        taskService.findTaskByIndex("100", null);
//    }
//
//    @Test(expected = IncorrectIndexException.class)
//    public void findTaskByIndexWithIncorrectIndexTest() throws Exception {
//        taskService.findTaskByIndex("100", -5);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void findTaskByNameWithEmptyUserIdTest() throws Exception {
//        taskService.findTaskByName("", "task");
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void findTaskByNameWithNullUserIdTest() throws Exception {
//        taskService.findTaskByName(null, "task");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void findTaskByNameWithEmptyNameTest() throws Exception {
//        taskService.findTaskByName("100", "");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void findTaskByNameWithNullNameTest() throws Exception {
//        taskService.findTaskByName("100", null);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void updateTaskByIdWithEmptyUserIdTest() throws Exception {
//        taskService.updateTaskById("", "100", "newName", "newDesc");
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void updateTaskByIdWithNullUserIdTest() throws Exception {
//        taskService.updateTaskById(null, "100", "newName", "newDesc");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void updateTaskByIdWithEmptyIdTest() throws Exception {
//        taskService.updateTaskById("100", "", "newName", "newDesc");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void updateTaskByIdWithNullIdTest() throws Exception {
//        taskService.updateTaskById("100", null, "newName", "newDesc");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void updateTaskByIdWithEmptyNameTest() throws Exception {
//        taskService.updateTaskById("100", "100", "", "newDesc");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void updateTaskByIdWithNullNameTest() throws Exception {
//        taskService.updateTaskById("100", "100", null, "newDesc");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void updateTaskByIdWithEmptyDescriptionTest() throws Exception {
//        taskService.updateTaskById("100", "100", "newName", "");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void updateTaskByIdWithNullDescriptionTest() throws Exception {
//        taskService.updateTaskById("100", "100", "newName", null);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void updateTaskByIndexWithEmptyUserIdTest() throws Exception {
//        taskService.updateTaskByIndex("", 1, "newName", "newDesc");
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void updateTaskByIndexWithNullUserIdTest() throws Exception {
//        taskService.updateTaskByIndex(null, 1, "newName", "newDesc");
//    }
//
//    @Test(expected = IncorrectIndexException.class)
//    public void updateTaskByIndexWithIncorrectIndexTest() throws Exception {
//        taskService.updateTaskByIndex("100", -1, "newName", "newDesc");
//    }
//
//    @Test(expected = IncorrectIndexException.class)
//    public void updateTaskByIndexWithNullIndexTest() throws Exception {
//        taskService.updateTaskByIndex("100", null, "newName", "newDesc");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void updateTaskByIndexWithEmptyNameTest() throws Exception {
//        taskService.updateTaskByIndex("100", 1, "", "newDesc");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void updateTaskByIndexWithNullNameTest() throws Exception {
//        taskService.updateTaskByIndex("100", 1, null, "newDesc");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void updateTaskByIndexWithEmptyDescriptionTest() throws Exception {
//        taskService.updateTaskByIndex("100", 1, "newName", "");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void updateTaskByIndexWithNullDescriptionTest() throws Exception {
//        taskService.updateTaskByIndex("100", 1, "newName", null);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void removeTaskByIdWithEmptyUserIdTest() throws Exception {
//        taskService.removeTaskById("", "task");
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void removeTaskByIdWithNullUserIdTest() throws Exception {
//        taskService.removeTaskById(null, "task");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void removeTaskByIdWithEmptyIdTest() throws Exception {
//        taskService.removeTaskById("100", "");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void removeTaskByIdWithNullIdTest() throws Exception {
//        taskService.removeTaskById("100", null);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void removeTaskByIndexWithEmptyUserIdTest() throws Exception {
//        taskService.removeTaskByIndex("", 0);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void removeTaskByIndexWithNullUserIdTest() throws Exception {
//        taskService.removeTaskByIndex(null, 0);
//    }
//
//    @Test(expected = IncorrectIndexException.class)
//    public void removeTaskByIndexWithNullIndexTest() throws Exception {
//        taskService.removeTaskByIndex("100", null);
//    }
//
//    @Test(expected = IncorrectIndexException.class)
//    public void removeTaskByIndexWithIncorrectIndexTest() throws Exception {
//        taskService.removeTaskByIndex("100", -5);
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void removeTaskByNameWithEmptyUserIdTest() throws Exception {
//        taskService.removeTaskByName("", "task");
//    }
//
//    @Test(expected = EmptyUserIdException.class)
//    public void removeTaskByNameWithNullUserIdTest() throws Exception {
//        taskService.removeTaskByName(null, "task");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void removeTaskByNameWithEmptyNameTest() throws Exception {
//        taskService.removeTaskByName("100", "");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void removeTaskByNameWithNullNameTest() throws Exception {
//        taskService.removeTaskByName("100", null);
//    }

}
