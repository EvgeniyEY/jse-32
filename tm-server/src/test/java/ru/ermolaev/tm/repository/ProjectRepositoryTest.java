package ru.ermolaev.tm.repository;

public class ProjectRepositoryTest {

    /*
        Требуется рефакторинг в связи с переходом на новую моель хранения данных
    */

//    final private IProjectRepository projectRepository = new ProjectRepository();
//
//    final private static ProjectDTO PROJECT_DTO_ONE = new ProjectDTO();
//
//    final private static ProjectDTO PROJECT_DTO_TWO = new ProjectDTO();
//
//    final private static ProjectDTO PROJECT_DTO_THREE = new ProjectDTO();
//
//    final private static ProjectDTO[] PROJECT_DTO_ARRAY = {PROJECT_DTO_ONE, PROJECT_DTO_TWO, PROJECT_DTO_THREE};
//
//    final private static List<ProjectDTO> PROJECT_DTO_LIST = Arrays.asList(PROJECT_DTO_ARRAY);
//
//    @BeforeClass
//    public static void initData() {
//        PROJECT_DTO_ONE.setName("project-1");
//        PROJECT_DTO_ONE.setUserId("123123123");
//
//        PROJECT_DTO_TWO.setName("project-2");
//        PROJECT_DTO_TWO.setUserId("123123123");
//
//        PROJECT_DTO_THREE.setName("project-3");
//        PROJECT_DTO_THREE.setUserId("789789789789");
//    }
//
//    @Before
//    public void addProject() {
//        projectRepository.add(PROJECT_DTO_ONE);
//    }
//
//    @After
//    public void deleteProject() {
//        projectRepository.clear();
//    }
//
//    @Test
//    public void findByIdTest() throws Exception {
//        final ProjectDTO projectDTO = projectRepository.findById(PROJECT_DTO_ONE.getUserId(), PROJECT_DTO_ONE.getId());
//        Assert.assertNotNull(projectDTO);
//        Assert.assertEquals(projectDTO.getName(), PROJECT_DTO_ONE.getName());
//    }
//
//    @Test
//    public void findByIndexTest() throws Exception {
//        final ProjectDTO projectDTO = projectRepository.findByIndex(PROJECT_DTO_ONE.getUserId(),0);
//        Assert.assertNotNull(projectDTO);
//        Assert.assertEquals(projectDTO.getName(), PROJECT_DTO_ONE.getName());
//    }
//
//    @Test
//    public void findByNameTest() throws Exception {
//        final ProjectDTO projectDTO = projectRepository.findByName(PROJECT_DTO_ONE.getUserId(), PROJECT_DTO_ONE.getName());
//        Assert.assertNotNull(projectDTO);
//        Assert.assertEquals(projectDTO.getName(), PROJECT_DTO_ONE.getName());
//    }
//
//    @Test
//    public void findAllTest() {
//        projectRepository.add(PROJECT_DTO_LIST);
//        final List<ProjectDTO> projectDTOS = projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId());
//        Assert.assertNotNull(projectDTOS);
//        Assert.assertEquals(projectDTOS.size(), 3);
//    }
//
//    @Test
//    public void removeByIdTest() throws Exception {
//        Assert.assertEquals(1, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//        projectRepository.removeById(PROJECT_DTO_ONE.getUserId(), PROJECT_DTO_ONE.getId());
//        Assert.assertEquals(0, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void removeByIndexTest() throws Exception {
//        Assert.assertEquals(1, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//        projectRepository.removeByIndex(PROJECT_DTO_ONE.getUserId(), 0);
//        Assert.assertEquals(0, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void removeByNameTest() throws Exception {
//        Assert.assertEquals(1, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//        projectRepository.removeByName(PROJECT_DTO_ONE.getUserId(), PROJECT_DTO_ONE.getName());
//        Assert.assertEquals(0, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void removeTest() {
//        Assert.assertEquals(1, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//        projectRepository.remove(PROJECT_DTO_ONE);
//        Assert.assertEquals(0, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void clearTest() {
//        projectRepository.add(PROJECT_DTO_LIST);
//        Assert.assertEquals(3, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//        projectRepository.clear(PROJECT_DTO_ONE.getUserId());
//        Assert.assertEquals(0, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//    }
//
//    @Test
//    public void addTest() throws Exception {
//        projectRepository.add(PROJECT_DTO_THREE);
//        final ProjectDTO projectDTO = projectRepository.findById(PROJECT_DTO_THREE.getUserId(), PROJECT_DTO_THREE.getId());
//        Assert.assertNotNull(projectDTO);
//        Assert.assertEquals(projectDTO.getName(), PROJECT_DTO_THREE.getName());
//    }
//
//    @Test
//    public void loadCollectionTest() {
//        Assert.assertEquals(1, projectRepository.findAll().size());
//        projectRepository.load(PROJECT_DTO_LIST);
//        Assert.assertEquals(3, projectRepository.findAll().size());
//    }
//
//    @Test
//    public void loadVarargsTest() {
//        Assert.assertEquals(1, projectRepository.findAll().size());
//        projectRepository.load(PROJECT_DTO_ARRAY);
//        Assert.assertEquals(3, projectRepository.findAll().size());
//    }
//
//    @Test(expected = UnknownIdException.class)
//    public void findByIdExceptionTest() throws Exception {
//        final ProjectDTO projectDTO = projectRepository.findById(PROJECT_DTO_ONE.getUserId(), "qweq");
//        Assert.assertNotNull(projectDTO);
//        Assert.assertEquals(projectDTO.getName(), PROJECT_DTO_ONE.getName());
//    }
//
//    @Test(expected = UnknownIndexException.class)
//    public void findByIndexExceptionTest() throws Exception {
//        final ProjectDTO projectDTO = projectRepository.findByIndex(PROJECT_DTO_ONE.getUserId(),-7);
//        Assert.assertNotNull(projectDTO);
//        Assert.assertEquals(projectDTO.getName(), PROJECT_DTO_ONE.getName());
//    }
//
//    @Test(expected = UnknownNameException.class)
//    public void findByNameExceptionTest() throws Exception {
//        final ProjectDTO projectDTO = projectRepository.findByName(PROJECT_DTO_ONE.getUserId(), "qqw");
//        Assert.assertNotNull(projectDTO);
//        Assert.assertEquals(projectDTO.getName(), PROJECT_DTO_ONE.getName());
//    }
//
//    @Test(expected = UnknownIdException.class)
//    public void removeByIdExceptionTest() throws Exception {
//        Assert.assertEquals(1, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//        projectRepository.removeById(PROJECT_DTO_ONE.getUserId(), "qwq");
//        Assert.assertEquals(0, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//    }
//
//    @Test(expected = UnknownIndexException.class)
//    public void removeByIndexExceptionTest() throws Exception {
//        Assert.assertEquals(1, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//        projectRepository.removeByIndex(PROJECT_DTO_ONE.getUserId(), -54);
//        Assert.assertEquals(0, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//    }
//
//    @Test(expected = UnknownNameException.class)
//    public void removeByNameExceptionTest() throws Exception {
//        Assert.assertEquals(1, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//        projectRepository.removeByName(PROJECT_DTO_ONE.getUserId(), "12wqe");
//        Assert.assertEquals(0, projectRepository.findAllProjects(PROJECT_DTO_ONE.getUserId()).size());
//    }

}
